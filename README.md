### Installation ###
*npm install*

*npm start*

### Type in browser ###
localhost:3000



#In the news
Webpart can show links to external news. 
Links sorted by article date. Links can open in new tab or not, it depends by property.

##Prerequisites
###Lists
    - In the news
###Content types
    - WM In The News Links
###Fields
    - Url
    - Article Date
    - Open in new tab
###WebParts
    - InNews

###Provisioning
    - Provisioning scripts create "InNewsLink" content type.
    - Provisioning scripts create "In the news" list. 
    - You can use "In the news" list or create new list with "InNewsLink" content type

###Settings
    - [Top Title] - by default "In the news"
    - [Select List] - required ot select. List with external news
    - [Items to show] - count of items that webpart should show (1..100) per page
    - [Allow paging] - No - will be active link "view more"; Yes - will be active button "view more" (for paging);
    - [Url to View more page] - have defaut value ({~site}/Pages/InNews.aspx). You can set another url