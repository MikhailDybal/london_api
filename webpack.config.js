var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var PATHS = {
    app: path.join(__dirname, '/src/app'),
    pages: path.join(__dirname, '/src/app/pages'),
    components: path.join(__dirname, '/src/app/components'),
};

var config = {
    context: path.join(__dirname, './src/app/'),
    entry: path.join(__dirname, './src/app/index.jsx'),
    output: {
        path: path.join(__dirname, './build'),
        publicPath: '/',
        filename: '[name].js',
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.css'],
        alias: {
            'app': PATHS.app,
            'components': PATHS.components,
            'pages': PATHS.pages,
        }
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: PATHS.app,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015', 'stage-2', 'react']
                }
            },
            {
                test: /\.json$/, loader: 'json'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    'style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader'
                )
            },
            {
                test: /\.svg$/,
                loader: "url-loader?limit=10000&mimetype=image/svg+xml"
            },
            { test: /\.(woff2?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=30000&name=fonts/[name]-[hash].[ext]' },
            { test: /\.(jpe?g|png|gif)$/i, loaders: [
                'file?hash=sha512&digest=hex&name=img/[hash].[ext]',
                'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
            ]}
        ]
    },

    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    },

    postcss: [
        require('autoprefixer'),
        require('postcss-size'),
        require('postcss-pxtorem'),
        require('precss')
    ],

    devServer: {
        inline: true,
        contentBase: './src/app',
        port: 3000,
        historyApiFallback: true,
    },

    plugins: [
        new ExtractTextPlugin('style.css', {allChunks: true}),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, './src/app/template.html'),
            title: 'london_parking',
            inject: 'body',
            filename: 'index.html'
        })
    ]
};

module.exports = config;