import React, {Component} from 'react';
import Layout from 'components/Layout/Layout';
import Spinner from 'components/Spinner/Spinner';
import isMobile from 'ismobilejs';
import _ from 'lodash';
import HomePage from '../home-page/home.page';
import {Router, browserHistory} from 'react-router';
import styles from './park-details.page.css';
import GoogleMap from 'components/GoogleMap/GoogleMap';
import {FaCheck, FaClose} from 'react-icons/lib/fa';

export default class ParkDetailsPage extends HomePage {
	constructor(props) {
		super(props);
        this.state = {
            parking: {},
            occupancy: {},
            errorLoading: false
        };
	}

    async loadParking(id) {
        let res = await fetch('https://api-argon.tfl.gov.uk/Place/'+encodeURIComponent(id))
            .then(response => {
                return response.json()
            })
            .then(parking =>{
                this.loadOccupancy(_.find(parking.additionalProperties,{'key':"OccupancyUrl"}).value);
                return parking
            })
            .catch(()=> {
                this.setState({
                    errorLoading: true,
                })
            });
        res = HomePage.getPreparedParkings([res])[0];
        this.setState({
            parking: res,
        })
    }

    async loadOccupancy(url) {
        let res = await fetch(url)
            .then(response => response.json())
            .catch(()=> {
                console.log('occupancy not found');
            });
        this.setState({
            occupancy: res,
        })
    }

    componentDidMount() {
        this.loadParking(this.props.params.id);
    }

    componentWillMount() {
    }

	render() {
        let renderIcon = (value)=> value ? <span><FaCheck className={styles.box__icon__green} /></span> : <span><FaClose className={styles.box__icon__orange} /></span>;
        let renderText = (value)=> value ? <span>{value}</span> : <span>Unknown</span>;
        let renderCosts = (value)=> value ? <span className={styles.text_costs}>${value}</span> : <span>Unknown</span>;
        let renderNumbers = (value)=> value ? <span className={styles.text_numbers}>{value}</span> : <span>Unknown</span>;

        return (
            <Layout>
                <div className={styles.map_wrap}>
                    <GoogleMap carParks={[this.state.parking]}/>
                </div>
                {!this.state.parking.id && !this.state.errorLoading && <Spinner />}
                {this.state.errorLoading && <h1>Not Found</h1>}

                {this.state.parking.id &&
                <div className={isMobile.any ? styles.m_wrap : styles.wrap}>
                    <h2>{renderText(this.state.parking.commonName)} </h2>
                    <p><b>Narrative: </b> {renderText(this.state.parking.narrative)} </p>
                    <p><b>Address: </b> {renderText(this.state.parking.address)} </p>
                    <p><b>Phone number: </b> {renderText(this.state.parking.tel)} </p>
                    <p><b>Methods Of Payment: </b> {renderText(this.state.parking.methodsOfPayment)} </p>
                    <p><b>Schedule: </b> {renderText(this.state.parking.schedule)} </p>
                    <hr/>
                    <p><b>Open Now: </b> {renderIcon(this.state.parking.open)} </p>
                    <p><b>Safer Parking: </b> {renderIcon(this.state.parking.saferParking)} </p>
                    <p><b>Season Tickets: </b> {renderIcon(this.state.parking.seasonTickets)} </p>
                    <p><b>Congestion Charge Zone: </b> {renderIcon(this.state.parking.congestionChargeZone)} </p>
                    <p><b>Car Electrical Charging Points: </b> {renderIcon(this.state.parking.carElectricalChargingPoints)} </p>
                    <hr/>
                    <p><b>Cost Saturday And Bank Holidays: </b> {renderCosts(this.state.parking.costSaturdayAndBankHolidays)} </p>
                    <p><b>Cost Season Tickets Annually: </b> {renderCosts(this.state.parking.costSeasonTicketsAnnually)} </p>
                    <p><b>Cost Season Tickets Daily: </b> {renderCosts(this.state.parking.costSeasonTicketsDaily)} </p>
                    <p><b>Cost Season Tickets Monthly: </b> {renderCosts(this.state.parking.costSeasonTicketsMonthly)} </p>
                    <p><b>Cost Season Tickets Quarterly: </b> {renderCosts(this.state.parking.costSeasonTicketsQuarterly)} </p>
                    <p><b>Cost Season Tickets Weekly: </b> {renderCosts(this.state.parking.costSeasonTicketsWeekly)} </p>
                    <p><b>Cost Standard Tariffs CashDaily: </b> {renderCosts(this.state.parking.costStandardTariffsCashDaily)} </p>
                    <p><b>Cost Standard Tariffs Cash Saturday And Bank Holiday: </b> {renderCosts(this.state.parking.costStandardTariffsCashSaturdayAndBankHoliday)} </p>
                    <p><b>Cost Standard Tariffs Cash Sunday: </b> {renderCosts(this.state.parking.costStandardTariffsCashSunday)} </p>
                    <p><b>Cost Standard Tariffs Cashless Daily: </b> {renderCosts(this.state.parking.costStandardTariffsCashlessDaily)} </p>
                    <p><b>Cost Standard Tariffs Cashless Sunday: </b> {renderCosts(this.state.parking.costStandardTariffsCashlessSunday)} </p>
                    <hr/>
                    <p><b>Spaces: </b> {renderNumbers(this.state.parking.spaces)} </p>

                    {this.state.occupancy.id && <p><b>Free {this.state.occupancy.bays[1].bayType}: </b> {renderNumbers(this.state.occupancy.bays[1].free)}</p>}
                </div>}
            </Layout>
        )
	}
};
