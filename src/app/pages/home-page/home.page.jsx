import React, {Component} from 'react';
import isMobile from 'ismobilejs';
import GoogleMap from 'components/GoogleMap/GoogleMap';
import Layout from 'components/Layout/Layout';
import ParksList from 'components/ParksList/ParksList';
import styles from './home.page.css';
import Filters from 'components/Filters/Filters';
import _ from 'lodash';

export default class HomePage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            carParks: [],
            filteredCarParks: [],
            filtersSettings:{},
            loaded: false
        };
        this.handleFiltersChange = this.handleFiltersChange.bind(this);
        this.filterParks = this.filterParks.bind(this);
    }

    async loadCarParks() {
        let res = await fetch("https://api.tfl.gov.uk/Place/Type/CarPark").then(response => response.json());
        res = HomePage.getPreparedParkings(res);
        this.setState({
            carParks: res,
            filteredCarParks: res,
            loaded: true
        })
    }

    componentDidMount() {
        this.loadCarParks();
    }

    handleFiltersChange(value) {
        this.filterParks(value);
    }

    static getPreparedParkings(parkings){
        let preparedParkings = [];
        for (let parking of parkings) {
            let preparedParking = {};

            //id
            preparedParking.id = (parking.id) ? parking.id : null;

            //commonName
            preparedParking.commonName = (parking.commonName) ? parking.commonName.replace(' Stn (LUL)', '') : null;

            //cords
            preparedParking.lat = parking.lat || null;
            preparedParking.lng = parking.lon || null;

            //address
            let addr1=_.find(parking.additionalProperties,{'key':"Address1"});
            let addr2=_.find(parking.additionalProperties,{'key':"Address2"});
            let addr3=_.find(parking.additionalProperties,{'key':"Address3"});
            let addr4=_.find(parking.additionalProperties,{'key':"Address4"});
            let address= (addr1 ? addr1.value : "");
            address+= (addr2 ? (", "+addr2.value) : "");
            address+= (addr3 ? (", "+addr3.value) : "");
            address+= (addr4 ? (", "+addr4.value) : "");
            preparedParking.address = address || null;

            //TelephoneNumber
            let phone = _.find(parking.additionalProperties,{'key':"TelephoneNumber"});
            preparedParking.phone = phone ? phone.value : null;

            //NumberOfSpaces
            let spaces = _.find(parking.additionalProperties,{'key':"NumberOfSpaces"});
            preparedParking.spaces = spaces ? Number(spaces.value) : null;

            //OpeningHours
            let schedule = _.find(parking.additionalProperties,{'key':"OpeningHours"});
            preparedParking.schedule = schedule ? schedule.value.replace(';', '') : null;

            //Open
            let open = _.find(parking.additionalProperties,{'key':"Open"});
            preparedParking.open = open ? JSON.parse(open.value.toLowerCase()) : false;

            //CongestionChargeZone
            let congestionChargeZone  = _.find(parking.additionalProperties,{'key':"CongestionChargeZone"});
            preparedParking.congestionChargeZone = congestionChargeZone ? JSON.parse(congestionChargeZone.value.toLowerCase()) : false;

            //SaferParking
            let saferParking  = _.find(parking.additionalProperties,{'key':"SaferParking"});
            preparedParking.saferParking = saferParking ? JSON.parse(saferParking.value.toLowerCase()) : false;

            //SeasonTickets
            let seasonTickets  = _.find(parking.additionalProperties,{'key':"SeasonTickets"});
            preparedParking.seasonTickets = seasonTickets ? JSON.parse(seasonTickets.value.toLowerCase()) : false;

            //CarElectricalChargingPoints
            let carElectricalChargingPoints  = _.find(parking.additionalProperties,{'key':"CarElectricalChargingPoints"});
            preparedParking.carElectricalChargingPoints = carElectricalChargingPoints ? JSON.parse(carElectricalChargingPoints.value.toLowerCase()) : false;

            //TelephoneNumber
            let tel = _.find(parking.additionalProperties,{'key':"TelephoneNumber"});
            preparedParking.tel = tel ? tel.value.toLowerCase() : null;

            //Narrative
            let narrative = _.find(parking.additionalProperties,{'key':"Narrative"});
            preparedParking.narrative = narrative ? narrative.value : null;

            //StandardTariffsCashDaily
            let standardTariffsCashDaily = _.find(parking.additionalProperties,{'key':"StandardTariffsCashDaily"});
            preparedParking.costStandardTariffsCashDaily = standardTariffsCashDaily ? Number(standardTariffsCashDaily.value) : null;

            //StandardTariffsCashSaturdayAndBankHoliday
            let standardTariffsCashSaturdayAndBankHoliday = _.find(parking.additionalProperties,{'key':"StandardTariffsCashSaturdayAndBankHoliday"});
            preparedParking.costStandardTariffsCashSaturdayAndBankHoliday = standardTariffsCashSaturdayAndBankHoliday ? Number(standardTariffsCashSaturdayAndBankHoliday.value) : null;

            //StandardTariffsCashSunday
            let standardTariffsCashSunday = _.find(parking.additionalProperties,{'key':"StandardTariffsCashSunday"});
            preparedParking.costStandardTariffsCashSunday = standardTariffsCashSunday ? Number(standardTariffsCashSunday.value) : null;

            //StandardTariffsCashlessDaily
            let standardTariffsCashlessDaily = _.find(parking.additionalProperties,{'key':"StandardTariffsCashlessDaily"});
            preparedParking.costStandardTariffsCashlessDaily = standardTariffsCashlessDaily ? Number(standardTariffsCashlessDaily.value) : null;

            //SaturdayAndBankHolidays
            let saturdayAndBankHolidays = _.find(parking.additionalProperties,{'key':"SaturdayAndBankHolidays"});
            preparedParking.costSaturdayAndBankHolidays = saturdayAndBankHolidays ? Number(saturdayAndBankHolidays.value) : null;

            //StandardTariffsCashlessSunday
            let standardTariffsCashlessSunday = _.find(parking.additionalProperties,{'key':"StandardTariffsCashlessSunday"});
            preparedParking.costStandardTariffsCashlessSunday = standardTariffsCashlessSunday ? Number(standardTariffsCashlessSunday.value) : null;

            //SeasonTicketsWeekly
            let seasonTicketsWeekly = _.find(parking.additionalProperties,{'key':"SeasonTicketsWeekly"});
            preparedParking.costSeasonTicketsWeekly = seasonTicketsWeekly ? Number(seasonTicketsWeekly.value) : null;

            //SeasonTicketsMonthly
            let seasonTicketsMonthly = _.find(parking.additionalProperties,{'key':"SeasonTicketsMonthly"});
            preparedParking.costSeasonTicketsMonthly = seasonTicketsMonthly ? Number(seasonTicketsMonthly.value) : null;

            //SeasonTicketsQuarterly
            let seasonTicketsQuarterly = _.find(parking.additionalProperties,{'key':"SeasonTicketsQuarterly"});
            preparedParking.costSeasonTicketsQuarterly = seasonTicketsQuarterly ? Number(seasonTicketsQuarterly.value) : null;

            //SeasonTicketsAnnually
            let seasonTicketsAnnually = _.find(parking.additionalProperties,{'key':"SeasonTicketsAnnually"});
            preparedParking.costSeasonTicketsAnnually = seasonTicketsAnnually ? Number(seasonTicketsAnnually.value) : null;

            //SeasonTicketsDaily
            let seasonTicketsDaily = _.find(parking.additionalProperties,{'key':"SeasonTicketsDaily"});
            preparedParking.costSeasonTicketsDaily = seasonTicketsDaily ? Number(seasonTicketsDaily.value) : null;

            //MethodsOfPayment
            let methodsOfPayment = _.find(parking.additionalProperties,{'key':"MethodsOfPayment"});
            preparedParking.methodsOfPayment = methodsOfPayment ? methodsOfPayment.value : null;

            //OccupancyUrl
            let occupancyUrl = _.find(parking.additionalProperties,{'key':"OccupancyUrl"});
            preparedParking.occupancyUrl = occupancyUrl ? occupancyUrl.value : null;

            //url
            preparedParking.url = parking.url || null;

            preparedParkings.push(preparedParking);
        }
        return preparedParkings;
    }

    filterParks(value){
        let filteredCarParks = this.state.carParks.filter(park=>(park.commonName && park.commonName.toLowerCase().indexOf(value.name.toLowerCase())>=0) ? true : false);
            filteredCarParks = filteredCarParks.filter(park=>(park.address && park.address.toLowerCase().indexOf(value.address.toLowerCase())>=0) ? true : false);

        filteredCarParks = (value.sortByName) ? _.sortBy(filteredCarParks, ['commonName']) : filteredCarParks;
        filteredCarParks = (value.sortByAddress) ? _.sortBy(filteredCarParks, ['address']) : filteredCarParks;
        filteredCarParks = (value.sortByCost) ? _.sortBy(filteredCarParks, ['costStandardTariffsCashDaily']) : filteredCarParks;
        this.setState({filteredCarParks: filteredCarParks});
    }

    render() {
        return (
            <Layout>
                <div className={isMobile.any ? styles.m_map : styles.map}>
                    <GoogleMap carParks={this.state.filteredCarParks}/>
                </div>
                <div className={isMobile.any ? styles.m_inner : styles.inner}>
                    <Filters onChange={this.handleFiltersChange}/>
                    <ParksList carParks={this.state.filteredCarParks} loaded={this.state.loaded}/>
                </div>
            </Layout>
        );
    }
}
