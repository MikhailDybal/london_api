import React from 'react';
import Layout from 'components/Layout/Layout';
import {BITBUCKET_LINK} from '../../preferences';
import isMobile from 'ismobilejs';
import styles from './about.page.css';

export default function AboutPage() {
    return(
        <Layout>
            <div className={isMobile.any ? styles.m_inner : styles.inner}>
                <p><a href="https://api.tfl.gov.uk/"><img src={require('../../shared/img/api.png')} alt="api.tfl.gov.uk"/></a></p>
                <p><a href={BITBUCKET_LINK}><img src={require('../../shared/img/bitbucket-logo.png')} alt={BITBUCKET_LINK}/></a></p>
                <p><b>Mikhail Dybal.</b></p>
                <p>Test React application. Desktop and Mobile design</p>
                <p>React, JSX, ES6, Webpack, PostCSS</p>
                <p><a href="mailto:michaeldybal@gmail.com">Email</a> </p>
            </div>
        </Layout>
    );
}
