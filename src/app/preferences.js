export const BITBUCKET_LINK = 'https://bitbucket.org/MikhailDybal/london_api';

export const MAP = {
    CENTER_POSITION: {lat: 51.576928, lng: -0.134822},
    ZOOM_LEVEL: 10,
    KEY: 'AIzaSyDAUk08kKAUXKMNWOivOhSS9TyGytLSAIw',
    VERSION: '3.exp'
};

export const NAV_ITEMS = [
    {url: '/', title: 'Home'},
    {url: '/about', title: 'About'},
];
