import React from 'react';
import {render} from 'react-dom';
import {Router, browserHistory} from 'react-router';
import getRoutes from './routes';
import './shared/utils/font.css';
import './shared/global.css';

render( 
    <Router history={browserHistory}>
        {getRoutes()}
    </Router>, document.getElementById('app')
);
