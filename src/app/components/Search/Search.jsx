import React, {PropTypes} from 'react';
import MdSearch from 'react-icons/lib/md/search';
import styles from './Search.css';

export default function Search(props) {
	return(
		<div className={props.bordered ? styles.input__wrap_bordered : styles.input__wrap}>
            <MdSearch size={21} className={styles.input__icon} />
            <input
              type="text"
              className={styles.input}
              value={props.value}
              placeholder={props.placeholder}
              onChange={props.onChange}
              autoFocus={props.autoFocus}
            />
        </div>
	);
}

Search.propTypes = {
	value: PropTypes.string,
	onChange: PropTypes.func,
	autoFocus: PropTypes.bool,
	placeholder: PropTypes.string,
};
