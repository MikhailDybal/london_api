import React from 'react';
import {FaFacebookSquare,  FaInstagram} from 'react-icons/lib/fa';
import styles from './Footer.css';
import isMobile from 'ismobilejs';
import {BITBUCKET_LINK} from '../../preferences';

export default function Footer() {
    return(
        <div className={isMobile.any ? styles.m_footer : styles.footer}>
            <div className={isMobile.any ? styles.m_inner : styles.inner}>
                <div className={styles.col}>
                    <div>MD Test React App</div>
                </div>
                <div className={styles.col}>
                    <div><a href={BITBUCKET_LINK}>Bitbucket</a></div>
                </div>
            </div>
        </div>
    );
}
