import React, {PropTypes, Component} from 'react';
import isMobile from 'ismobilejs';
import Header from 'components/Header/Header';
import MobileNav from 'components/Nav/MobileNav/MobileNav';
import Footer from 'components/Footer/Footer';
import styles from './Layout.css';

export default class Layout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuShow: false
        };

        this.toggleNav = this.toggleNav.bind(this);
    }

    toggleNav() {
        if(this.state.isMenuShow) {
            this.setState({isMenuShow: false});
        } else {
            this.setState({isMenuShow: true});
        }
    }

    render() {
        return (
            <div className={isMobile ? styles.m_layout : styles.layout}>
                {(this.state.isMenuShow && isMobile.any) && <MobileNav />}
                <div className={this.state.isMenuShow ? styles.wrap_open : styles.wrap}>
                    <Header
                        toggleNav={this.toggleNav}
                        isMenuShow={this.state.isMenuShow}
                    />
                    <div className={styles.content}>
                        {this.props.children}
                    </div>
                    {<Footer />}
                </div>
            </div>
        );
    }
}
