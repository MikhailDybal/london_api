import React, {Component} from 'react';
import {Gmaps, Marker,InfoWindow} from 'react-gmaps';
import {MAP} from '../../preferences';

export default class GoogleMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infoWindow: null
        };
    }

    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true
        });
    }

    onCloseClick() {
        this.setState({infoWindow: null});
    }

    onClick(carPark, e) {
        this.setState({infoWindow: <InfoWindow
            content={`
                      <h3>${carPark.commonName}</h3>
                      <b>Address: </b>${carPark.address}<br>
                      <b>Phone number: </b>${carPark.phone}<br>
                      <b>Spaces count: </b>${carPark.spaces}<br>
                      <b>Schedule: </b>${carPark.schedule}<br>
                      <b>Work: </b>${carPark.open ? "Open Now" : "Closed Now"}<br>
                      <p><a href="/parking/${carPark.id}">Show more...</a></p>
            `}
            lat={carPark.lat}
            lng={carPark.lng}
            onCloseClick={this.onCloseClick.bind(this)}
        />});
    }

    render() {
        return(
           <Gmaps
               width={'100%'}
               height={'100%'}
               lat={MAP.CENTER_POSITION.lat}
               lng={MAP.CENTER_POSITION.lng}
               zoom={MAP.ZOOM_LEVEL}
               loadingMessage={'Please wait..'}
               params={{v: MAP.VERSION, key: MAP.KEY}}
               onMapCreated={this.onMapCreated.bind(this)}>

               {this.props.carParks && this.props.carParks.map((carPark, i) =>
                   <Marker
                       key={carPark.id+i}
                       lat={carPark.lat}
                       lng={carPark.lng}
                       draggable={false}
                       icon={require('../../shared/img/marker_parking.png')}
                       onClick={this.onClick.bind(this, carPark)}
                   />
                )}{this.state.infoWindow}
           </Gmaps>
        );
    };
}
