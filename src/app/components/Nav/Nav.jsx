import React from 'react';
import {NAV_ITEMS} from '../../preferences';
import styles from './Nav.css';
import {Link} from 'react-router';
 
export default function Nav() {
    return (
        <nav className={styles.nav}>
            {NAV_ITEMS.map((item, i) =>
                <Link to={item.url} key={i}>
                    <span  className={styles.item}>{item.title}</span>
                </Link>)}
        </nav>
    );
}
