import React from 'react';
import {NAV_ITEMS} from '../../../preferences';
import styles from './MobileNav.css';
import {Link} from 'react-router';

export default function MobileNav() {
    return (
        <nav className={styles.wrap}>
            <div className={styles.header}>
                Menu
            </div>
            <ul className={styles.list}>
                {NAV_ITEMS.map((item, i) => <li key={i}>
                    <Link to={item.url}>
                        <span className={styles.link}>{item.title}</span>
                    </Link>

                </li>)}
            </ul>
        </nav>
    );
}
