import React from 'react';
import styles from './Spinner.css';

export default function Spinner(props) {
    return(
        <div className={styles.spinner} />
    );
};
