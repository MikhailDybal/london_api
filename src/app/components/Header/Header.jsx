import styles from './Header.css';
import React, {PropTypes} from 'react';
import {MdMenu, MdClose} from 'react-icons/lib/md';
import {Link} from 'react-router';
import isMobile from 'ismobilejs';
import Logo from 'components/Logo/Logo';
import Nav from 'components/Nav/Nav';

export default function Header(props) {
    return (
        <div className={styles.header}>
            <div className={isMobile.any ? styles.m_inner : isMobile.any ? styles.m_inner : styles.inner}>
                {isMobile.any && <button className={styles.toggle_nav} onClick={props.toggleNav}>
                    {props.isMenuShow ? <MdClose size="24" /> : <MdMenu size="24" />}
                </button>}
                <div className={isMobile.any ? styles.center : styles.left}>
                    <Link to={'/'}>
                        <span className={styles.logo}>
                            <Logo header />
                        </span>
                    </Link>
                    {!isMobile.any && <Nav />}
                </div>
            </div>
        </div>
    );
}

Header.propTypes = {
    toggleNav: PropTypes.func,
    isMenuShow: PropTypes.bool,
};
