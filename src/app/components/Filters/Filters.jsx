import React, {Component} from 'react';
import {FaCaretDown, FaCaretUp} from 'react-icons/lib/fa';
import styles from './Filters.css';
import {FILTER_FIELDS} from '../../preferences';
import FilterBtn from './FilterBtn/FilterBtn';
import Search from 'components/Search/Search';

export default class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = this.getFiltersObject();
        this.openFilters = this.openFilters.bind(this);
        this.closeFilters = this.closeFilters.bind(this);
        this.renderSearchByName = this.renderSearchByName.bind(this);
        this.renderSearchByAddress = this.renderSearchByAddress.bind(this);
        this.handleInputChangeName = this.handleInputChangeName.bind(this);
        this.handleInputChangeAddress = this.handleInputChangeAddress.bind(this);
        this.handleClickFilter = this.handleClickFilter.bind(this);
        this.handleResetFilters = this.handleResetFilters.bind(this);
    }

    getFiltersObject(){
        return {
            isOpen: false,
            name: '',
            address: '',
            sortByName: false,
            sortByAddress: false,
            sortByCost: false,
        }
    }

    openFilters() {
        this.setState({ isOpen: true });
    }

    closeFilters() {
        this.setState({ isOpen: false });
    }

    handleResetFilters() {
        this.state = this.getFiltersObject();
        this.props.onChange(this.state);
    }

    handleInputChangeName(e) {
        this.state.name = e.target.value;
        this.props.onChange(this.state);
    }

    handleInputChangeAddress(e) {
        this.state.address = e.target.value;
        this.props.onChange(this.state);
    }

    handleClickFilter(e, value) {
        this.state[e.toString()] = value;
        this.props.onChange(this.state);
    }

    renderSearchByName() {
        return (
            <Search
                value={this.state.name}
                placeholder="Enter Parking Name"
                onChange={this.handleInputChangeName}
                autoFocus={true}
            />
        );
    }

    renderSearchByAddress() {
        return (
            <Search
                value={this.state.address}
                placeholder="Enter Address"
                onChange={this.handleInputChangeAddress}
                autoFocus={false}
            />
        );
    }

    render() {
        return(
            <div className={styles.wrap}>
                <div className={styles.box}>
                    {this.state.isOpen ? this.renderSearchByName() : <div className={styles.title} onClick={this.openFilters}>Search &amp; filters</div>}
                    {this.state.isOpen ? this.renderSearchByAddress() : ""}
                    {this.state.isOpen ?
                        <FaCaretUp className={styles.box__icon} onClick={this.closeFilters} /> :
                        <FaCaretDown className={styles.box__icon} onClick={this.openFilters} />
                    }
                </div>

                {this.state.isOpen && <div className={styles.form}>

                        <div className={styles.form__group}>
                            <div className={styles.form__label}>Sort by:</div>
                            <div className={styles.form__fields}>
                                <FilterBtn label='Name' id="sortByName" onClick={this.handleClickFilter} value={this.state.sortByName}/>
                                <FilterBtn label='Address' id="sortByAddress" onClick={this.handleClickFilter} value={this.state.sortByAddress}/>
                                <FilterBtn label='Cost' id="sortByCost" onClick={this.handleClickFilter} value={this.state.sortByCost}/>
                            </div>
                        </div>

                    <div className={styles.form__footer}>
                        <button type="button" className={styles.form__submit} onClick={this.handleResetFilters}>Reset</button>
                    </div>
                </div>}

            </div>
        );
    }
}
