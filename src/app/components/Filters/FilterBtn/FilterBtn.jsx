import React, {Component, PropTypes} from 'react';
import styles from './FilterBtn.css';
import classnames from 'classnames/bind';

let cx = classnames.bind(styles);

export default class FilterBtn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checked: props.value
		};
		this.toggle = this.toggle.bind(this);
		this.getBtnClasses = this.getBtnClasses.bind(this);
	} 

	toggle(e) {
		if(this.state.checked) {
			this.setState({ checked: false });
		}else {
			this.setState({ checked: e.target.checked });
		}
        this.props.onClick(this.props.id, !this.state.checked)
	}

	getBtnClasses() {
		let styles = cx({
			btn: true,
			active: this.state.checked,
		});
		return styles;
	}

	render() {
		return(
			<label className={this.getBtnClasses()} onChange={this.toggle}>
				<input type="checkbox" checked={this.state.checked} className={styles.checkbox} />
				{this.props.label}
			</label>
		);
	}
}

FilterBtn.propTypes = {
	label: PropTypes.string,
	onClick: PropTypes.func,
    value: PropTypes.bool,
};
