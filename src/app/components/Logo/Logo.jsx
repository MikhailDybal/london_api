import React, {PropTypes} from 'react';

export default function Logo(props) {
    return (
        <img src={require('../../shared/img/logo.png')} alt="London Car Parking" height="50px"/>
    );
};

Logo.propTypes = {
    light: PropTypes.bool,
};

