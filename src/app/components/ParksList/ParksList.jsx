import React, {PropTypes} from 'react';
import Spinner from 'components/Spinner/Spinner';
import ParkItem from './ParkItem/ParkItem';
import styles from './ParksList.css';

export default function ParksList(props) {
    let {carParks, loaded} = props;

    return (
        <div className={styles.wrap}>
            {!carParks.length && loaded && <h2>{'Nothing not found'}</h2>}
            {!loaded && <Spinner />}
            {carParks && carParks.map(carPark => <ParkItem key={carPark.id} carPark={carPark}/>)}
        </div>
    );
}

ParksList.propTypes = {
    carParks: PropTypes.array.isRequired,
    loaded: PropTypes.bool,
};
