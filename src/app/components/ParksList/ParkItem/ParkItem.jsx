import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import isMobile from 'ismobilejs';
import styles from './ParkItem.css';

export default function ParkItem(props) {
    let {carPark} = props;
    return (
        <div>
            {carPark &&
            <Link to={`/parking/${carPark.id}`} className={isMobile.any ? styles.m_box : styles.box}>
                <div>
                    <h2>{carPark.commonName}</h2>
                    <p><b>Address: </b>{carPark.address}</p>
                    <p><b>Methods of payment: </b>{carPark.methodsOfPayment}</p>
                    <p><b>Schedule: </b>{carPark.schedule}</p>
                    <p><b>Open: </b>{carPark.open ? "Open Now" : "Closed Now"}</p>
                    <p><b>Cost standard tariffs cash daily: </b>{carPark.costStandardTariffsCashDaily ? (<b>$ {carPark.costStandardTariffsCashDaily}</b>) : "Unknown"}</p>
                </div>
            </Link>}
        </div>
    );
}

ParkItem.propTypes = {
    carPark: PropTypes.object,
};
