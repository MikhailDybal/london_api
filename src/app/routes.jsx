import React from 'react';
import {Route, IndexRoute} from 'react-router';
import HomePage from './pages/home-page/home.page';
import ParkDetailsPage from 'pages/park-details-page/park-details.page';
import AboutPage from 'pages/about-page/about.page';

export default function getRoutes() {
    return (
        <Route path="/">
            <IndexRoute component={HomePage} />
            <Route name="parking" path="/parking/:id" component={ParkDetailsPage} />
            <Route name="about" path="/about" component={AboutPage} />
        </Route>
    )
}
